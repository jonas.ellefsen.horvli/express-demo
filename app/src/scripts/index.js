import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'
import Post from './post'
import User from './user';

/**
 * Global Constants
 */
const elPosts = $('.posts');
const elUser = $('.user');
const btnSearch = $('#btn-search');
const elSearchBox = $('#searchbox');

/**
 * Variables
 */
let posts = [];

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function () {
    // Fetch posts from the api and display them on the screen.
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        method: 'GET',
        error: error => {
            console.log('error()', error);
        }
    }).done(data => {
        posts = [];
        $(data).each((index, post) => {
            const newPost = new Post(post);
            posts.push(newPost);
            const elPost = $('<p></p>').text(newPost.title).attr('class', 'post');
            const elPoint = $('<li></li>').append(elPost);
            $(elPost).click(() => {
                clickPost(newPost);
            });
            $(elPosts).append(elPoint);
        })
    });
    btnSearch.click(() => {
        filterPosts(elSearchBox.val())
    })
});

/**
 * Show selected post.
 * Fetch the data of the user who created the selected post from the api.
 * @param {object} post 
 */
function showPost(post) {
    $('.user').empty();
    const backButton = $('<button/>').text('back');
    backButton.attr('id', 'backButton');
    $('.user').append(backButton);
    backButton.click(() => clickBack());
    $.ajax({
        url: `https://jsonplaceholder.typicode.com/users/${post.userId}`,
        method: 'GET',
        error: error => {
            console.log('error()', error);
        }
    }).done(data => {
        const user = new User(data);
        let userInfo =
`Display name: ${user.getUserName()}
Name: ${user.getName()}
E-mail: ${user.getEmail()}
Phone: ${user.getPhone()}`
        const title = $('<h4></h4>').text(post.title);
        const body = $('<p></p>').text(post.body);
        body.attr('class', 'postBody');
        const info = $('<p></p>').text(userInfo);
        $(elUser).append(title);
        $(elUser).append(body);
        $(elUser).append(info);
    })
}

function clickBack() {
    hidePost();
    togglePosts();
}

function clickPost(post) {
    togglePosts();
    showPost(post);
}

function hidePost() {
    $('.user').empty();
}

function togglePosts() {
    $(elPosts).toggle();
    $('.row').toggle();
    filterPosts('');
    elSearchBox.val('');
}

/**
 * Filter the list by searched word.
 * @param {string} text 
 */
function filterPosts(text) {
    $(elPosts).empty();
    posts.forEach(post => {
        if (post.title.includes(text)) {
            const elPost = $('<p></p>').text(post.title).attr('class', 'post');
            const elPoint = $('<li></li>').append(elPost);
            $(elPost).click(() => {
                clickPost(post);
            });
            $(elPosts).append(elPoint);
        }
    })
}

/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
function createCard(url, title, body) {
    return `
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                ${url
                    ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}

                <div class="card-body">
                    <h5>${title}</h5>
                    ${body.split('\n').map(para => `<p>${para}</p>`).join('\n')}
                </div>
            </div>
        </div>
    `;
}